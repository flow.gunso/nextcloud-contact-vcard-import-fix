#!/usr/bin/env python
import argparse
import re
import quopri

# Setup the argument parser.
parser = argparse.ArgumentParser(description = "Fix a vCard file before importing in Nextcloud, which require valid vCard 3.0")
parser.add_argument('file', type=str, help='vCard file to fix')
args = parser.parse_args()

""" Quick heads up:
Read a vCard file (.vcf) to find and fix issues 
which prevent from importing the file in Nextcloud Contact 
(require VERSION:3.0, N: and FN:).
"""

# Read the file
with open(args.file) as file:

    # Init the fixed filename.
    filename = str(args.file)
    extension_index = filename.find(".")
    fixed_filename = filename[:extension_index] + "-fixed" + filename[extension_index:]
    lines = file.readlines()

    # Use a manual loop, more more control over it is required.
    line_index = 0
    while line_index < len(lines):
        # Placeholder for the property value.
        value = lines[line_index]

        # New VCARD reached, reset the properties.
        if re.match("^(BEGIN:VCARD)$", value):
            n = None
            fn = None
            version = None
            version_index = None
            properties = []

        # VCARD end reached, check the properties then rewrite to a new file.
        elif re.match("^(END:VCARD)$", value):

            # Check the whole entry for being empty or not.
            ignore_vcards = False
            if len(properties) is 1 and re.match("(VERSION:).*", properties[0]):
                ignore_vcards = True
            elif len(properties) is 0:
                ignore_vcards = True

            # Process non-empty vCard only.
            if ignore_vcards is False:

                # Check the properties.
                if n is None:
                    properties.append("N: \n")
                if fn is None:
                    properties.append("FN: \n")
                if version is not "3.0":
                    properties.pop(version_index)
                    properties.append("VERSION:3.0\n")

                # Write the vCard to the fixed file.
                with open(fixed_filename, "a") as output:
                    output.write("BEGIN:VCARD\n")
                    for newline in properties:
                        output.write(newline)
                    output.write("END:VCARD\n")

        # Otherwise, read the properties.
        else:

            # Some properties may be ignored.
            ignore_property = False

            # Find and convert utf8 to ascii.
            if re.match(".*(CHARSET=UTF-8;ENCODING=QUOTED-PRINTABLE:).*", value):
                value = value.replace("CHARSET=UTF-8;ENCODING=QUOTED-PRINTABLE:", "")
                value = quopri.decodestring(value)
                value = value.decode("utf-8")

            # Extract the version when found.
            if re.match("^(VERSION:).*", value):
                version = value.strip("VERSION:")
                version_index = len(properties)

            # Set N: and FN: as True when found.
            elif re.match("^(N:).*", value):
                n = True
            elif re.match("^(FN:).*", value):
                fn = True

            # PHOTO: property seems to fail the import, lets ignore them to test that out.
            elif re.match("^(PHOTO).*(:).*", value):
                ignore_property = True
                # Fast forward to the next property.
                while True:
                    line_index+=1
                    value = lines[line_index]
                    # Property value is continued, so do we.
                    if re.match("^( ).*", value):
                        continue
                    # Empty line reached, next is a new property.
                    elif not value.strip():
                        break
                    # New property reached, set index as previous line.
                    elif re.match("^.*(:)", value):
                        line_index-=1
                        break

            # Store fixed/valid properties.
            if ignore_property is False:
                properties.append(value)

        line_index+=1