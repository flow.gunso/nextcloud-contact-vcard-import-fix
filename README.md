# Nextcloud Contacts vCard import fix

Importing vCard file in Nextcloud's Contact app is still tidious, at least with the version I have.
This script fix the following issues in a vCard file:
- VERSION must be at 3.0
- N must be present
- FN must be present
- Discard empty vCard entries
- Discard PHOTO properties
- Decode any UTF-8 to ASCII

### To Do
- Check if the file is really a vCard file
- When generating the fixed filename, manage paths